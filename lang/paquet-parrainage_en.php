<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-parrainage?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'parrainage_description' => 'Allows you to import contacts and send mass invitations to join the site. If people sign up, then a link is maintained with the one who invited them (the sponsor).',
	'parrainage_slogan' => 'Provide users with a tool for sponsorship.'
);
