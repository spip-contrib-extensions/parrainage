<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/parrainage?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_filleul_confirmation' => 'The contact has been added, you can invite that person.',

	// C
	'configuration_delai_sans_nouvelles_label' => 'Period for the status "No news"',
	'configuration_invitation_obligatoire_label' => 'Mandatory invitation',
	'configuration_titre' => 'Sponsorship configuration',

	// E
	'erreur_aucun_contact' => 'You have not imported any contacts',
	'erreur_aucun_filleul' => 'You have not selected any sponsored person.',
	'erreur_inscription_desactivee' => 'Registration to the site has been disactivated. You cannot sponsor anyone.',
	'erreur_invitation_invalide' => 'Your invitation does not match the email address provided.',
	'erreur_invitation_obligatoire' => 'You need an invitation to register on the site.',

	// F
	'filleul_actions' => 'Actions',
	'filleul_ajouter' => 'Add',
	'filleul_email' => 'E-mail',
	'filleul_nom' => 'Name',
	'filleul_statut' => 'Status',

	// I
	'inscription_code_invitation_label' => 'Invitation code',
	'invitation_message' => '@nom_parrain@ invites you to join the site @site@.',
	'invitation_sujet' => '@nom@ invits you to join the website @site@',
	'invitation_url' => 'Go to the following address to register :',

	// M
	'message_insciption_fermee' => 'Warning, registration to the site is closed. You must activate it in order to use this plugin.',
	'message_ouvrir_visiteur' => 'If you would like to open registration to new visitors, you can activate this option below (configuring the plugin "Sponsoship"sends new subscribers an invitation code) :',

	// P
	'parrainage_inviter' => 'Send invitation',
	'parrainage_message_aucun' => 'None of these people needs to be invited.',
	'parrainage_message_aucun_1' => 'This person is already invited or registered.',
	'parrainage_message_erreur' => 'At least one invitation was not sent correctly.',
	'parrainage_message_label' => 'Your message',
	'parrainage_message_ok_pluriel' => '@nombre@ invitations have been programmed, they are now being send.',
	'parrainage_message_ok_singulier' => 'An invitation has been programmed, it is now being send.',
	'parrainage_supprime_nb' => '@nb@ deleted contacts',
	'parrainage_supprime_un' => '@nb@ deleted contact',
	'parrainage_supprimer_filleul' => 'Delete this contact',
	'parrainage_supprimer_filleul_confirmation' => 'Are you sure you want to delete @nom@ from your contacts ?',
	'parrainage_supprimer_filleuls' => 'Delete the selected contacts',
	'parrainage_supprimer_filleuls_confirmation' => 'Are you sure you want to delete the selected contacts?',
	'plugin_nom' => 'Sponsorship',

	// S
	'selectionner_rien' => 'Deselect all',
	'selectionner_tout' => 'Select all',
	'statut_contact' => 'Contact',
	'statut_contact_explication' => 'This person is in your address book. You can send an invitation to join the site.',
	'statut_deja_inscrit' => 'Already registered',
	'statut_deja_inscrit_explication' => 'This person is already registered on the site, but is not sponsored by you !',
	'statut_en_cours' => 'Invitation being send',
	'statut_en_cours_explication' => 'Your invitation message is being send.',
	'statut_filleul' => 'Sponsored person',
	'statut_filleul_depuis' => 'Sponsored person registered since @date@',
	'statut_filleul_explication' => 'Thanks to you, this person registered on the site : you are his sponsor.',
	'statut_invite' => 'Invited @duree@',
	'statut_invite_explication' => 'You have already invited this person some time ago.',
	'statut_sans_nouvelles' => 'No news since @date@',
	'statut_sans_nouvelles_explication' => 'You have invited this person some time ago, but he never came.',
	'statut_visite' => 'Visited the site',
	'statut_visite_explication' => 'This person has already received your invitation to come to the site, but is not yet registered.'
);
