<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/parrainage?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_filleul_confirmation' => 'El contacto se ha añadido correctamente, ya puede invitar a esta persona. ',

	// C
	'configuration_delai_sans_nouvelles_label' => 'Plazo para el estatus &quot;Sans nouvelles&quot;',
	'configuration_invitation_obligatoire_label' => 'Invitación obligatoria',
	'configuration_titre' => 'Configuración del apadrinamiento',

	// E
	'erreur_aucun_contact' => 'No ha importado ningún contacto.',
	'erreur_aucun_filleul' => 'No ha seleccionado ningún ahijado.',
	'erreur_inscription_desactivee' => 'La inscripción al sitio está desactivada. No puede por tanto apadrinar a nadie. ',
	'erreur_invitation_invalide' => 'Su invitación no corresponde a la dirección de correo indicada.',
	'erreur_invitation_obligatoire' => 'Ha de tener una invitación para registrarse en el sitio.',

	// F
	'filleul_actions' => 'Acciones',
	'filleul_ajouter' => 'Añadir',
	'filleul_email' => 'Correo electrónico',
	'filleul_nom' => 'Nombre',
	'filleul_statut' => 'Estatus',

	// I
	'inscription_code_invitation_label' => 'Código de invitación',
	'invitation_message' => '@nom_parrain@ le invita a participar en el sitio @site@.',
	'invitation_sujet' => '@nom@ le invita a participar en el sitio @site@',
	'invitation_url' => 'Vaya a la siguiente dirección para registrarse:',

	// M
	'message_insciption_fermee' => 'Atención, la inscripción al sitio está cerrada. Debe activarla para utilizar este plugin',
	'message_ouvrir_visiteur' => 'Si desea abrir el registro al sitio a nuevos visitantes, debe activar la siguiente opción (la configuración del plugin "Apadrinamiento" obliga al nuevo registrado a beneficiarse de un código de invitación):',

	// P
	'parrainage_inviter' => 'Enviar la invitación',
	'parrainage_message_aucun' => 'Ninguna de estas personas necesita ser invitada.',
	'parrainage_message_aucun_1' => 'Esta persona ya está invitada o registrada.',
	'parrainage_message_erreur' => 'Al menos una invitación no se ha enviado correctamente.',
	'parrainage_message_label' => 'Su mensaje',
	'parrainage_message_ok_pluriel' => '@nombre@ invitaciones se ha programado correctamente, están ahora en curso de envío.',
	'parrainage_message_ok_singulier' => 'Una invitación se ha programado correctamente, está ahora en curso de envío. ',
	'parrainage_supprime_nb' => '@nb@ contactos eliminados',
	'parrainage_supprime_un' => '@nb@ contacto eliminado',
	'parrainage_supprimer_filleul' => 'Eliminar este contacto',
	'parrainage_supprimer_filleul_confirmation' => '¿Está seguro de querer eliminar a @nom@ de sus contactos?',
	'parrainage_supprimer_filleuls' => 'Eliminar los contactos seleccionados',
	'parrainage_supprimer_filleuls_confirmation' => '¿Está seguro de querer eliminar los contactos seleccionados de sus contactos?',
	'plugin_nom' => 'Apadrinamiento',

	// S
	'selectionner_rien' => 'Desmarcar todo',
	'selectionner_tout' => 'Marcar todo',
	'statut_contact' => 'Contacto',
	'statut_contact_explication' => 'Esta persona está en su agenda direcciones. Puede invitarla a unirse al sitio.',
	'statut_deja_inscrit' => 'Ya registrado',
	'statut_deja_inscrit_explication' => 'Esta persona ya está registrada en el sitio, ¡pero no es su ahijada!',
	'statut_en_cours' => 'Invitación en curso de envío',
	'statut_en_cours_explication' => 'Su mensaje de invitación está en curso de expedición.',
	'statut_filleul' => 'Ahijado',
	'statut_filleul_depuis' => 'Ahijado registrado desde @date@',
	'statut_filleul_explication' => 'Gracias a usted, esta persona está registrada en el sitio: usted es su padrino.',
	'statut_invite' => 'Invitado @duree@',
	'statut_invite_explication' => 'Ya ha invitado a esta persona hace poco tiempo.',
	'statut_sans_nouvelles' => 'Sin noticias desde @date@',
	'statut_sans_nouvelles_explication' => 'Ha invitado a esta persona hace algún tiempo, pero jamás ha venido.',
	'statut_visite' => 'Ha visitado el sitio',
	'statut_visite_explication' => 'Esta persona ya ha recibido su invitación para venir al sitio, pero aún no se ha registrado.'
);
